import java.util.Random;
import java.util.Scanner;

public class BattleShip {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int[][] gameBoard = new int[5][5];
        int targetRow = random.nextInt(5);
        int targetCol = random.nextInt(5);
        boolean gamewin = false;

        System.out.println("All set. Get ready to rumble!");

        while (!gamewin) {
            System.out.print("Enter the row for fire 1-5: ");
            int row = getUserInput(scanner);
            System.out.print("Enter the column for fire 1-5 : ");
            int col = getUserInput(scanner);
            if (row - 1 == targetRow && col - 1 == targetCol) {
                gameBoard[targetRow][targetCol] = 1;
                gamewin = true;
            } else {
                System.out.println("You missed");
                gameBoard[row - 1][col - 1] = -1;
            }
            printGameBoard(gameBoard);
        }
        System.out.println("You have won!!!");
        gameBoard[targetRow][targetCol] = 2;
        printGameBoard(gameBoard);
        scanner.close();
    }


    public static int getUserInput(Scanner scanner) {
        while (!scanner.hasNextInt()) {
            System.out.println("Please enter the number");
            scanner.next();
        }
        return scanner.nextInt();
    }

    public static void printGameBoard(int[][] gameBoard) {
        for (int i = 0; i < gameBoard.length; i++) {
            for (int j = 0; j < gameBoard.length; j++) {
                if (gameBoard[i][j] == 1) {
                    System.out.print("x ");
                } else if (gameBoard[i][j] == -1) {
                    System.out.print("* ");
                } else {
                    System.out.print("- ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}


